const jwt = require( 'jsonwebtoken' );
const ERRORS = require( '../../config/errors' );


const errorHandler = ( error, req, res, next ) => {
    if ( error instanceof jwt.TokenExpiredError ) {
        return res.status( 401 ).send( {
            success: false,
            errors: ERRORS.TOKEN_EXPIRED(),
            data: null
        } );
    }

    if ( error instanceof jwt.JsonWebTokenError ) {
        return res.status( 401 ).send( {
            success: false,
            errors: ERRORS.INVALID_TOKEN(),
            data: null
        } );
    }

    if ( Object.keys( ERRORS ).includes( error.message ) ) {
        const { id, message, code } = ERRORS[error.message]();

        return res.status( code || 400 ).send( {
            success: false,
            errors: { id, message },
            data: null
        } );
    }

    if ( error.type === 'entity.parse.failed' ) {
        return res.status( error.statusCode ).send( {
            success: false,
            errors: {
                id: 'INVALID_JSON',
                message: error.message
            },
            data: null
        } );
    }

    console.log( error );
    return res.status( 500 ).send( {
        success: false,
        errors: ERRORS.SERVER_ERROR( error ),
        data: null
    } );
};

module.exports = { errorHandler };
