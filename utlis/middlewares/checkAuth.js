const jwt = require( 'jsonwebtoken' );
const dotenv = require( '../dotenv' );
const { badRequestHandler } = require( '../helpers/errorHandler' );

module.exports = ( req, res, next ) => {
    const authHeader = req.get( 'Authorization' );
    if ( !authHeader ) {
        return badRequestHandler( 'TOKEN_NOT_PROVIDED', req, res );
    }

    let payload = {};
    const token = authHeader.replace( 'Bearer ', '' );
    try {
        payload = jwt.verify( token, dotenv.parsed.JWT_SECRET );

        const allowedTypes = [ 'PERSONAL_ACCESS_TOKEN', 'access' ];

        if ( !allowedTypes.includes( payload.type ) ) {
            return badRequestHandler( 'INVALID_TOKEN', req, res );
        }
    } catch ( error ) {
        if ( error instanceof jwt.TokenExpiredError ) {
            return badRequestHandler( 'TOKEN_EXPIRED', req, res );
        }
        if ( error instanceof jwt.JsonWebTokenError ) {
            return badRequestHandler( 'INVALID_TOKEN', req, res );
        }
    }
    req.token = token;
    req.tokenData = payload;
    next();
};
