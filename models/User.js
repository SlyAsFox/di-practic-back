const { Model, DataTypes } = require( 'sequelize' );
const sequelize = require( '../sequelize' );

class User extends Model {}

User.init( {
    id: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: true,
        default: ''
    },
    email: {
        type: DataTypes.STRING,
        allowNull: true
        // validate: {
        //     isEmail: true
        // }
    },
    password: {
        type: DataTypes.STRING,
        allowNull: true
    }
}, {
    sequelize,
    modelName: 'users',
    underscored: true,
    timestamps: true,
    indexes: [
        {
            name: 'email',
            fields: [ 'email' ]
        }
    ]
} );

module.exports = User;
