const ERRORS = {
    VALIDATION_DATA_ERROR: ( errors ) => {
        return {
            id: 'VALIDATION_DATA_ERROR',
            message: JSON.stringify( errors )
        };
    },
    TOKEN_EXPIRED: () => {
        return {
            id: 'TOKEN_EXPIRED',
            message: 'Token expired!',
            code: 401
        };
    },
    USER_NOT_FOUND: () => {
        return {
            id: 'USER_NOT_FOUND',
            message: 'User with provided id not found.',
            code: 400
        };
    },
    USER_ALREAY_EXIST: () => {
        return {
            id: 'USER_ALREAY_EXIST',
            message: 'User with provided email user alreay exist.',
            code: 400
        };
    },
    PROVIDER_NOT_FOUND: () => {
        return {
            id: 'PROVIDER_NOT_FOUND',
            message: 'Provider with provided id not found.',
            code: 400
        };
    },
    ICE_CREAM_NOT_FOUND: () => {
        return {
            id: 'ICE_CREAM_NOT_FOUND',
            message: 'Ice cream with provided id not found.',
            code: 400
        };
    },
    INVALID_TOKEN: ( message ) => {
        return {
            id: 'INVALID_TOKEN',
            message: message || 'Invalid token!',
            code: 401
        };
    },
    INVALID_CREDENTIALS: ( message ) => {
        return {
            id: 'INVALID_CREDENTIALS',
            message: message || 'INVALID_CREDENTIALS',
            code: 401
        };
    },
    SERVER_ERROR: ( error ) => {
        return {
            id: 'SERVER_ERROR',
            message: `Something went wrong: \n${error.message}`
        };
    },
};

module.exports = ERRORS;
