const sequelize = require( './sequelize' );
const { server } = require( './app' );
const { PORT } = require( './dotenv' );

sequelize.authenticate()
    .then( () => {
        server.listen( PORT );
        console.log( `[dev branch]Server started at PORT: ${PORT}` );
    } )
    .catch( err => {
        console.log( `Connection ERROR: ${err}` );
        throw err;
    } );
