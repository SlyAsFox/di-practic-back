const { NODE_ENV, DB_NAME, DB_PASSWORD, DB_HOST } = require( './dotenv' );
const Sequlize = require( 'sequelize' );

console.log( `ENV: ${NODE_ENV}` );



const sequelize = new Sequlize(
    DB_NAME,
    DB_NAME,
    DB_PASSWORD,
    {
        host: DB_HOST,
        dialect: 'postgres',
        logging: false, // console.log,
        benchmark: true,
        pool: {
            max: 20,
            min: 0,
            acquire: 60000,
            idle: 60000
        }
    }
);

module.exports = sequelize;
