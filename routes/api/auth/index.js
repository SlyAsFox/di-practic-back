const router = require('express').Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {User} = require('../../../models'); // Оновіть шлях відповідно до вашої структури
const JWT_SECRET = 'your_secret_key';
const asyncHandler = require( 'express-async-handler' );
const uuid = require('uuid').v4;

router.post( '/signup', asyncHandler( async ( req, res ) => {
    const { email, password, name } = req.body;

    const existUser = await User.findOne({ where: { email } });

    if( existUser ){
        throw new Error('USER_ALREAY_EXIST')
    }

    const hashedPassword = await bcrypt.hash(password, 10);

    const user = await User.create({
        id: uuid(),
        name,
        email,
        password: hashedPassword
    });

    return res.status(201).send(user);
} ) );

router.post( '/signin', asyncHandler( async ( req, res ) => {
    const { email, password } = req.body;
    let user = await User.findOne({ where: { email } });

    if( !user ){
        throw new Error('INVALID_CREDENTIALS')
    }
    user = user.toJSON()

    console.log( 'user', user )
    console.log( 'user.password', user.password )

    if( !await bcrypt.compare(password, user.password) ){
        throw new Error('INVALID_CREDENTIALS')
    }

    const token = jwt.sign({ id: user.id }, JWT_SECRET, { expiresIn: '2d' });

    delete user.password;

    return res.send({
        token,
        user
    });
} ) );

module.exports = router;
