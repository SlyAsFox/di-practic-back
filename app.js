const http = require( 'http' );
const express = require( 'express' );
const cors = require( 'cors' );
const helmet = require( 'helmet' );
const asyncHandler = require( 'express-async-handler' );
const { errorHandler } = require( './utlis/middlewares/errorHandler' );
require( 'colors' );
const app = express();
app.enable( 'trust proxy' );
app.use( cors() );
app.use( helmet() );

// require( './sequelize' );

app.use( ( req, res, next ) => {
    console.log( `${req.method},AppVersion => ${req.get( 'AppVersion' )} | ${req.url}`.red );
    next();
} );

app.use( express.urlencoded( {
    extended: true
} ) );
app.use( express.json() );


app.get( '/healthyCheck', asyncHandler( async ( req, res ) => {
    const { token, tokenData: payload } = req;
    console.log( 'HEALTHY CHECK' );

    return res
        .status( 200 )
        .send( {
            status: 'OK',
            IP: req.ip,
            jwt: {
                token,
                payload
            }
        } );
} ) );

// Create HTTP server.
const server = http.createServer( app );

const authRoutes = require( './routes/api/auth' );
app.use( '/api/auth', authRoutes );

app.use( errorHandler );

module.exports = { server, app };
